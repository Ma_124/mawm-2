/// A tag layout.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Layout {
    /// A single window takes up the available space.
    Monocle,

    /// Split space between windows equally.
    Equal,

    /// Split the screen in a part for the main window and a part where all other windows are stacked.
    MainAndStack,

    /// Let all windows float.
    Floating,
}

impl Default for Layout {
    fn default() -> Self {
        Self::Equal
    }
}

/// Cardinal directions.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    /// Up.
    Up,

    /// Down.
    Down,

    /// Left.
    Left,

    /// Right.
    Right,
}
