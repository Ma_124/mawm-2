//! # Usage
//! 1. Create a new [`Wm`] using a [`DisplayServer`] provided by the dependant crate (e.g. [mawm-x11]).
//! 2. Enter an event loop and call the appropriate functions on the [`Wm`].
//!
//! [mawm-x11]: https://gitlab.com/Ma_124/mawm-2/-/tree/main/mawm-x11

#![warn(clippy::allow_attributes_without_reason)]
#![warn(clippy::cargo_common_metadata)]
#![warn(clippy::cognitive_complexity)]
#![warn(clippy::inconsistent_struct_constructor)]
#![warn(clippy::semicolon_if_nothing_returned)]
#![warn(clippy::use_self)]
#![warn(missing_docs)]

use std::fmt::Debug;
use std::hash::Hash;

use thiserror::Error;

pub mod config;
mod geometry;
mod layout;
mod wm;

pub use crate::geometry::*;
pub use crate::layout::*;
pub use crate::wm::*;

/// A display server.
///
/// For an example implementation see the [mawm-x11] crate.
///
/// [mawm-x11]: https://gitlab.com/Ma_124/mawm-2/-/tree/main/mawm-x11
pub trait DisplayServer: Debug {
    /// An error type for this display server.
    type Error: DisplayError;

    /// A handle to a window.
    type Window: Debug + Copy + Clone + PartialEq + Eq + Hash;

    /// A handle to a monitor.
    type MonitorId: Debug + Copy + Clone + PartialEq + Eq + Hash;

    /// Name of the backend
    fn name(&self) -> &str;

    /// The geometry of all monitors combined.
    fn entire_geometry(&self) -> Result<Geometry, Self::Error>;

    /// Show a client window.
    fn show_client(&mut self, w: Self::Window) -> Result<(), Self::Error>;

    /// Focus a client window.
    fn focus(&mut self, w: Self::Window) -> Result<(), Self::Error>;
}

/// An error produced by a [`DisplayServer`].
pub trait DisplayError: std::error::Error {}

/// A [`Result`] with a [`DisplayServer::Error`].
pub type WmResult<T, D> = Result<T, WmError<<D as DisplayServer>::Error>>;

/// An error encountered in the [`Wm`].
#[derive(Debug, Error)]
pub enum WmError<E: DisplayError> {
    /// An error produced by the [`DisplayServer`].
    #[error("display error: {0}")]
    DisplayError(#[from] E),
}
