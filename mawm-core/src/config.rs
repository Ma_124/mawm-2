//! [`Config`]

use crate::{Layout, TagId};

// use for doc links
#[allow(unused_imports)]
use crate::{Tag, Workspace};

/// The config.
#[derive(Debug, Default)]
pub struct Config {
    /// Workspaces to create.
    pub workspaces: Vec<WorkspaceConfig>,

    /// Tags to create.
    pub tags: Vec<TagConfig>,

    /// Default layout.
    pub default_layout: Option<Layout>,

    /// The fallback tag when no other tag is focused.
    pub default_tag: Option<TagId>,
}

/// A [`Workspace`] template.
#[derive(Debug)]
pub struct WorkspaceConfig {
    /// The numeric id of the workspace.
    pub id: u32,

    /// The display name of the workspace.
    pub name: String,

    /// The x coordinate of top left corner
    /// in X root window coordinates.
    ///
    /// Default: 0
    pub x: Option<i32>,

    /// The y coordinate of top left corner
    /// in X root window coordinates.
    ///
    /// Default: 0
    pub y: Option<i32>,

    /// Width in pixels.
    ///
    /// Default: Width of entire X root window.
    pub width: Option<i32>,

    /// Height in pixels.
    ///
    /// Default: Height of entire X root window.
    pub height: Option<i32>,
    // TODO EDID name for hot plugging
}

/// A [`Tag`] template.
#[derive(Debug)]
pub struct TagConfig {
    /// The numeric id of the tag.
    pub id: u32,

    /// The display name of the tag.
    pub name: String,
}
