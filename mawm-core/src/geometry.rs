/// Geometry ([`Coordinates`] and [`Dimensions`]).
#[derive(Debug)]
pub struct Geometry {
    /// Position.
    pub pos: Coordinates,

    /// Size.
    pub size: Dimensions,
}

/// Coordinates.
#[derive(Debug)]
pub struct Coordinates {
    /// `x` (horizontal, left - right) component.
    pub x: i32,

    /// `y` (vertical, top - bottom) component.
    pub y: i32,
}

/// Dimensions.
#[derive(Debug)]
pub struct Dimensions {
    /// Width.
    pub w: i32,

    /// Height.
    pub h: i32,
}

/// Hints about the size the client desires.
#[derive(Debug, Default)]
pub struct SizeHints {
    /// Minimum size.
    pub min_size: Option<Dimensions>,

    /// Maximum size.
    pub max_size: Option<Dimensions>,

    /// Minimal and maximal aspect ratio after subtracting [`Self::size_base`].
    pub min_max_aspect_ratio: Option<(Ratio, Ratio)>,

    /// Base size for calculations involving [`Self::min_max_aspect_ratio`] and [`Self::size_increment`].
    pub size_base: Option<Dimensions>,

    /// Increment size.
    ///
    /// Width and heigt increments on top of [`Self::size_base`] (or [`Self::min_size`]).
    pub size_increment: Option<Dimensions>,
}

/// A ratio between two [`i32`].
pub type Ratio = fraction::Ratio<i32>;
