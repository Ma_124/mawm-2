//! Tag related actions of [`Wm`].

use std::{collections::BTreeMap, fmt::Display};

use tracing::{info, warn};

use crate::{config::Config, DisplayServer, Layout, Wm, WmResult};

/// A map of [`Tag`]s.
pub type Tags<D> = BTreeMap<TagId, Tag<D>>;

/// A tag id.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TagId(u32);

/// A tag, a collection of windows.
#[derive(Debug)]
pub struct Tag<D: DisplayServer> {
    /// The display name of the tag.
    pub name: String,

    /// Windows on this tag.
    pub windows: Vec<D::Window>,

    /// Layout of this tag.
    pub layout: Layout,

    /// The window currently focused on this tag.
    pub focused_window: Option<D::Window>,
}

impl<D: DisplayServer> Wm<D> {
    pub(super) fn setup_tags(_display: &D, cfg: &Config) -> WmResult<(Tags<D>, TagId), D> {
        let mut tags = BTreeMap::new();
        let layout = cfg.default_layout.unwrap_or_default();
        #[allow(clippy::needless_late_init)]
        let mut default_tag;

        if !cfg.tags.is_empty() {
            for tag in &cfg.tags {
                tags.insert(
                    TagId(tag.id),
                    Tag {
                        name: tag.name.clone(),
                        windows: Vec::new(),
                        layout,
                        focused_window: None,
                    },
                );
            }

            default_tag = TagId(cfg.tags[0].id);
        } else {
            for i in 1..=9 {
                tags.insert(
                    TagId(i),
                    Tag {
                        name: i.to_string(),
                        windows: Vec::new(),
                        layout,
                        focused_window: None,
                    },
                );
            }

            default_tag = TagId(1);
        }

        if let Some(tag) = cfg.default_tag {
            if tags.contains_key(&tag) {
                default_tag = tag;
            } else {
                warn!(%tag, "unknown default tag");
            }
        }

        Ok((tags, default_tag))
    }

    /// Set whether a tag is visible.
    #[tracing::instrument]
    pub fn set_tag_visible(&mut self, _t: TagId, _vis: bool) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// Set the layout on a tag.
    #[tracing::instrument]
    pub fn set_tag_layout(&mut self, _t: TagId, _layout: Layout) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// Set whether a window is on a tag.
    #[tracing::instrument]
    pub fn set_client_on_tag(&mut self, _w: D::Window, _t: TagId) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }
}

impl Display for TagId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
