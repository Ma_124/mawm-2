use enumflags2::{bitflags, BitFlags};
use tracing::{debug, info};

use crate::{
    geometry::{Geometry, SizeHints},
    DisplayServer, Wm, WmResult,
};

/// A client connected to the WM.
#[derive(Debug)]
pub struct Client<D: DisplayServer> {
    /// [`DisplayServer`]'s handle to the window.
    pub window: D::Window,

    /// App id of the client.
    pub app_id: String,

    /// Instance id of the client.
    pub instance_id: String,

    /// PID of the process producing this window.
    pub process: u32,

    /// Client title.
    pub title: String,

    /// Client type.
    pub typ: ClientType,

    /// Parent window.
    pub parent: Option<D::Window>,

    /// Boolean properties of this client.
    pub properties: ClientProperties,

    /// Current geometry of the window.
    pub geometry: Geometry,

    /// Geometry this window had when it was last floating.
    pub old_geometry: Option<Geometry>,

    /// Hints about the size the client desires.
    pub size_hints: SizeHints,

    /// Main window in group.
    pub group_leader: Option<D::Window>,
}

/// A collection of [`ClientProperty`]s.
pub type ClientProperties = BitFlags<ClientProperty>;

/// Boolean property of a [`Client`].
#[bitflags]
#[repr(u32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum ClientProperty {
    /// Window is managed by the window manager.
    Managed,

    /// Window is visible on screen
    /// but it may still be obscured by other windows.
    Visible,

    /// Window is a modal dialog box.
    Modal,

    /// Window is maximized.
    Maximized,

    /// Window is minimized.
    Minimized,

    /// Window is fullscreen.
    Fullscreen,

    /// Window should be shown above normal windows.
    AboveNormal,

    /// Window should be shown below normal windows.
    BelowNormal,

    /// Window demands attention.
    DemandsAttention,

    /// Window contains urgent content.
    Urgent,

    /// Window has a fixed size.
    Fixed,

    /// Window is floating or was floating before being fullscreen or maximized.
    Floating,

    /// Window should be shown on all tags.
    Sticky,

    /// Window should never be focused.
    NeverFocus,
}

impl<D: DisplayServer> Wm<D> {
    /// Manage a client.
    #[tracing::instrument]
    pub fn manage(&mut self, c: Client<D>) -> WmResult<(), D> {
        debug!(wid = ?c.window, title = %c.title, "manage client");

        if !c.properties.contains(ClientProperty::Managed) || self.clients.contains_key(&c.window) {
            return Ok(());
        }

        // TODO apply rules

        let active_workspace = &self.workspaces.get(&self.focused_workspace).unwrap();

        self.tags
            .get_mut(
                active_workspace
                    .active_tags
                    .first()
                    .unwrap_or(&self.default_tag),
            )
            .unwrap()
            .windows
            .push(c.window);

        let focus_result = if !c.properties.contains(ClientProperty::Minimized) {
            self.display.show_client(c.window)?;
            self.focus(c.window, self.focused_workspace)
        } else {
            Ok(())
        };

        self.clients.insert(c.window, c);
        focus_result?;
        Ok(())
    }

    /// Unmanage a client.
    #[tracing::instrument]
    pub fn unmanage(&mut self, w: D::Window) -> WmResult<(), D> {
        self.clients.remove(&w);

        for tag in self.tags.values_mut() {
            tag.windows.retain(|e| *e != w);

            if let Some(focused_window) = tag.focused_window {
                if focused_window == w {
                    // TODO focus history
                    tag.focused_window = None;
                }
            }
        }

        Ok(())
    }

    /// Close a window.
    #[tracing::instrument]
    pub fn close_window(&mut self, _w: D::Window) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// Set properties of a window.
    #[tracing::instrument]
    pub fn set_client_properties(
        &mut self,
        _w: D::Window,
        _props: ClientProperties,
    ) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The [`Client::title`] for a window changed.
    #[tracing::instrument]
    pub fn client_title_changed(&mut self, _w: D::Window, _title: String) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The [`Client::typ`] for a window changed.
    #[tracing::instrument]
    pub fn client_type_changed(&mut self, _w: D::Window, _typ: ClientType) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The [`Client::parent`] for a window changed.
    #[tracing::instrument]
    pub fn client_parent_changed(
        &mut self,
        _w: D::Window,
        _parent: Option<D::Window>,
    ) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The [`Client::geometry`] for a window changed.
    #[tracing::instrument]
    pub fn client_geometry_changed(
        &mut self,
        _w: D::Window,
        _geometry: Geometry,
    ) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// A client requested a [`Client::geometry`] change.
    #[tracing::instrument]
    pub fn client_geometry_change_requested(
        &mut self,
        _w: D::Window,
        _geometry: Geometry,
    ) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The [`Client::size_hints`] for a window changed.
    #[tracing::instrument]
    pub fn client_size_hints_changed(
        &mut self,
        _w: D::Window,
        _hints: SizeHints,
    ) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }
}

/// Client type.
#[derive(Debug, Clone, Copy)]
pub enum ClientType {
    /// Normal window.
    ///
    /// Default for unmanaged windows and windows without parents.
    Normal,

    /// Dialog window.
    ///
    /// Default for managed windows with a [`Client::parent`].
    Dialog,

    /// Window is part of the desktop.
    Desktop,

    /// Window is a dock.
    ///
    /// Should be always-on-top.
    Dock,

    /// Splash screen displayed during app startup.
    Splash,

    /// Pinnable toolbar "torn off" from the parent window.
    Toolbar,

    /// Pinnable menu "torn off" from the parent window.
    Menu,

    /// Persistent utility for the parent window.
    Utility,

    /// Dropdown menu.
    ///
    /// Typically unmanaged.
    DropdownMenu,

    /// Popup menu.
    ///
    /// Typically unmanaged.
    PopupMenu,

    /// Tooltip.
    ///
    /// Typically unmanaged.
    Tooltip,

    /// Notification.
    ///
    /// Typically unmanaged.
    Notification,

    /// Combo-box / completion dropdown.
    ///
    /// Typically unmanaged.
    Combo,

    /// Dragged icon.
    ///
    /// Typically unmanaged.
    DragNDrop,
}
