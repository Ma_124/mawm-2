use std::{collections::HashMap, fmt::Debug};

use crate::{config::Config, DisplayServer, WmResult};

mod clients;
mod focus;
mod tags;
mod workspaces;

pub use self::clients::*;
pub use self::focus::*;
pub use self::tags::*;
pub use self::workspaces::*;

/// The window manager.
pub struct Wm<D: DisplayServer> {
    /// The display server.
    pub display: D,

    /// All clients managed by the window manager.
    pub clients: HashMap<D::Window, Client<D>>,

    /// All workspaces.
    pub workspaces: Workspaces<D>,

    /// All tags.
    pub tags: Tags<D>,

    /// The workspace currently focused.
    pub focused_workspace: WorkspaceId,

    /// The fallback tag when no other tag is focused.
    pub default_tag: TagId,
}

impl<D: DisplayServer> Wm<D> {
    /// Create a new [`Wm`].
    pub fn new(display: D, cfg: &Config) -> WmResult<Self, D> {
        let (workspaces, focused_workspace) = Self::setup_workspaces(&display, cfg)?;
        let (tags, default_tag) = Self::setup_tags(&display, cfg)?;

        Ok(Self {
            display,
            clients: HashMap::default(),
            workspaces,
            tags,
            focused_workspace,
            default_tag,
        })
    }
}

impl<D: DisplayServer> Wm<D> {
    /// Return the name of the window manager (`mawm`).
    pub fn name(&self) -> &str {
        "mawm"
    }
}

impl<D: DisplayServer> Debug for Wm<D> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if f.alternate() {
            f.debug_struct("Wm")
                .field("display", &self.display)
                .field("clients", &self.clients)
                .field("workspaces", &self.workspaces)
                .field("tags", &self.tags)
                .field("focused_workspace", &self.focused_workspace)
                .field("default_tag", &self.default_tag)
                .finish()
        } else {
            write!(f, "Wm<{}>", self.display.name())
        }
    }
}
