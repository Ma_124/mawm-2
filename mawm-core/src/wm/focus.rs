//! Handle focus related actions and events for [`Wm`].

use tracing::info;

use crate::{Coordinates, DisplayServer, Wm, WmResult, WorkspaceId};

// use for doc links
#[allow(unused_imports)]
use crate::Workspace;

impl<D: DisplayServer> Wm<D> {
    /// Focus client on the given [`Workspace`] if it's visible there.
    #[tracing::instrument]
    pub fn focus(&mut self, w: D::Window, space: WorkspaceId) -> WmResult<(), D> {
        for tag in &self.workspaces.get(&space).unwrap().active_tags {
            let tag = self.tags.get_mut(tag).unwrap();
            if tag.windows.contains(&w) {
                tag.focused_window = Some(w);
                self.display.focus(w)?;
            }
        }

        Ok(())
    }

    /// Focus changed to another window.
    #[tracing::instrument]
    pub fn focus_changed(&mut self, _w: D::Window) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// Client wants to be focused.
    #[tracing::instrument]
    pub fn client_wants_focus(&mut self, _w: D::Window) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The mouse moved.
    #[tracing::instrument]
    pub fn mouse_moved(&mut self, _p: Coordinates) -> WmResult<(), D> {
        //info!("TODO");
        Ok(())
    }

    /// The mouse clicked.
    #[tracing::instrument]
    pub fn mouse_clicked(&mut self, _p: Coordinates) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The mouse entered a window.
    #[tracing::instrument]
    pub fn mouse_entered_client(&mut self, _w: D::Window) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// The mouse left a window.
    #[tracing::instrument]
    pub fn mouse_left_client(&mut self) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }
}
