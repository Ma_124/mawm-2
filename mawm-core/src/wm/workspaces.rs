//! Workspace management for [`Wm`].

use std::{collections::HashMap, fmt::Display, marker::PhantomData};

use tracing::info;

use crate::{
    config::Config, Coordinates, Dimensions, DisplayServer, Geometry, TagId, Wm, WmResult,
};

/// A map of [`Workspace`]s.
pub type Workspaces<D> = HashMap<WorkspaceId, Workspace<D>>;

/// A workspace id.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct WorkspaceId(u32);

/// A workspace, an area in which tags can be displayed.
#[derive(Debug)]
pub struct Workspace<D: DisplayServer> {
    pd: PhantomData<D>,

    /// The display name of the workspace.
    pub name: String,

    /// Geometry of this workspace.
    pub geometry: Geometry,

    /// Tags shown on this workspace.
    ///
    /// The first tag in the list is the main tag
    /// which controls the layout etc.
    pub active_tags: Vec<TagId>,
}

impl<D: DisplayServer> Wm<D> {
    pub(super) fn setup_workspaces(
        display: &D,
        cfg: &Config,
    ) -> WmResult<(Workspaces<D>, WorkspaceId), D> {
        let mut workspaces = HashMap::new();

        let root_geometry = display.entire_geometry()?;

        let focused_workspace;

        if !cfg.workspaces.is_empty() {
            focused_workspace = WorkspaceId(cfg.workspaces[0].id);
            for space in &cfg.workspaces {
                workspaces.insert(
                    WorkspaceId(space.id),
                    Workspace {
                        pd: PhantomData::default(),
                        name: space.name.clone(),
                        geometry: Geometry {
                            pos: Coordinates {
                                x: space.x.unwrap_or(0),
                                y: space.y.unwrap_or(0),
                            },
                            size: Dimensions {
                                w: space.width.unwrap_or(root_geometry.size.w),
                                h: space.height.unwrap_or(root_geometry.size.h),
                            },
                        },
                        active_tags: Vec::new(),
                    },
                );
            }
        } else {
            // TODO read randr monitors

            let id = WorkspaceId(0);
            let space = Workspace {
                pd: PhantomData::default(),
                name: "Main".to_owned(),
                geometry: root_geometry,
                active_tags: Vec::new(),
            };
            workspaces.insert(id, space);
            focused_workspace = id;
        }

        Ok((workspaces, focused_workspace))
    }

    /// The screen geometry changed.
    #[tracing::instrument]
    pub fn screen_geometry_changed(&mut self, _g: Geometry) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// A monitor was connected.
    #[tracing::instrument]
    pub fn monitor_connected(&mut self, _m: ()) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }

    /// A monitor was disconnected.
    #[tracing::instrument]
    pub fn monitor_disconnected(&mut self, _m: D::MonitorId) -> WmResult<(), D> {
        info!("TODO");
        Ok(())
    }
}

impl Display for WorkspaceId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
