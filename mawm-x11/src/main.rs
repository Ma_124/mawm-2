//! Provides an [implementation][XDisplay] of [`DisplayServer`] for X based on x11rb and drives the [`Wm`] using an event loop.

#![warn(clippy::allow_attributes_without_reason)]
#![warn(clippy::cargo_common_metadata)]
#![warn(clippy::cognitive_complexity)]
#![warn(clippy::inconsistent_struct_constructor)]
#![warn(clippy::semicolon_if_nothing_returned)]
#![warn(clippy::use_self)]
#![warn(missing_docs)]

pub mod atoms;
pub mod client;
pub mod properties;

use std::{
    borrow::Cow,
    fmt::Debug,
    io,
    process::{self, exit},
};

use atoms::Atoms;
use clap::Parser;
use client::{handle_client_message, handle_property_notify, new_client};
use mawm_core::{
    config::Config, Client, ClientProperty, Coordinates, Dimensions, DisplayError, DisplayServer,
    Geometry, Wm, WmError, WmResult,
};
use properties::{
    AtomListProperty, CardinalProperty, StringProperty, WindowProperty, WmClassProperty,
};
use thiserror::Error;
use tracing::{debug, error, trace, warn};
use tracing_subscriber::EnvFilter;
use x11rb::{
    connection::Connection,
    protocol::{
        xproto::{
            AtomEnum, ChangeWindowAttributesAux, ConnectionExt, CreateWindowAux, EventMask,
            InputFocus, NotifyDetail, NotifyMode, Window, WindowClass,
        },
        Event,
    },
    rust_connection::{ConnectError, ConnectionError, ReplyError, ReplyOrIdError, RustConnection},
};

/// The implementation of [`DisplayServer`] for X.
pub struct XDisplay {
    /// Connection to the X server.
    conn: RustConnection,

    /// The root window (covers all monitors).
    root_window: Window,

    /// The numeric ID's of various [`Atom`][x11rb::protocol::xproto::Atom]s,
    /// others can be found in [`AtomEnum`][x11rb::protocol::xproto::AtomEnum].
    atoms: Atoms,
}

impl DisplayServer for &XDisplay {
    type Error = XError;

    type Window = Window;

    type MonitorId = u32;

    fn name(&self) -> &str {
        "X11"
    }

    fn entire_geometry(&self) -> Result<Geometry, Self::Error> {
        let geo = self
            .conn
            .get_geometry(self.root_window)?
            .reply()
            .map_err(|err| XError::ReplyError {
                err,
                func: "get geometry of root window".into(),
            })?;

        Ok(Geometry {
            pos: Coordinates {
                x: geo.x.into(),
                y: geo.y.into(),
            },
            size: Dimensions {
                w: geo.width.into(),
                h: geo.height.into(),
            },
        })
    }

    fn show_client(&mut self, w: Self::Window) -> Result<(), Self::Error> {
        self.conn.map_window(w)?;
        Ok(())
    }

    fn focus(&mut self, w: Self::Window) -> Result<(), Self::Error> {
        self.conn.set_input_focus(InputFocus::NONE, w, 0u32)?;
        Ok(())
    }
}

impl XDisplay {
    /// Create a new [`XDisplay`] from the results of [`x11rb::connect`].
    pub fn new(conn: RustConnection, screen_num: usize) -> Result<Self, FatalError> {
        let atoms = Atoms::new(&conn)
            .map_err(XError::from)
            .map_err(FatalError::XError)?
            .reply()
            .map_err(|err| XError::ReplyError {
                err,
                func: "intern atoms".into(),
            })
            .map_err(FatalError::XError)?;

        Ok(Self {
            root_window: conn.setup().roots[screen_num].root,
            conn,
            atoms,
        })
    }

    /// Register as a window manager.
    fn register_wm(&self, wm: &Wm<&Self>, no_redirect: bool) -> Result<(), XError> {
        self.conn
            .change_window_attributes(
                self.root_window,
                &ChangeWindowAttributesAux::new().event_mask(
                    if no_redirect {
                        EventMask::NO_EVENT
                    } else {
                        EventMask::SUBSTRUCTURE_REDIRECT // MapRequest, ConfigureRequest
                    } | EventMask::SUBSTRUCTURE_NOTIFY // MapNotify, ConfigureNotify, DestroyNotify, UnmapNotify
                        | EventMask::STRUCTURE_NOTIFY // ConfigureNotify on root
                        | EventMask::ENTER_WINDOW // EnterNotify on root
                        | EventMask::LEAVE_WINDOW // LeaveNotify on root
                        | EventMask::PROPERTY_CHANGE // PropertyChange on root
                        | EventMask::POINTER_MOTION // MotionNotify on root
                        | EventMask::BUTTON_PRESS // ButtonPress on root
                        | EventMask::FOCUS_CHANGE, // FocusIn (TODO everywhere?)
                ),
            )?
            .check()
            .map_err(|err| XError::ReplyError {
                err,
                func: "register event mask (is another WM running?)".into(),
            })?;

        let support_win = self.conn.generate_id()?;
        self.conn
            .create_window(
                0,
                support_win,
                self.root_window,
                -1,
                -1,
                1,
                1,
                0,
                WindowClass::INPUT_ONLY,
                0,
                &CreateWindowAux::new(),
            )?
            .check()
            .map_err(|err| XError::ReplyError {
                err,
                func: "create support window".into(),
            })?;

        self.set_prop(
            support_win,
            self.atoms._NET_WM_PID,
            CardinalProperty,
            &process::id(),
        )?;
        self.set_prop(
            support_win,
            AtomEnum::WM_CLASS,
            WmClassProperty,
            &(wm.name().to_owned(), wm.name().to_owned()),
        )?;
        self.set_prop(
            support_win,
            self.atoms._NET_WM_NAME,
            StringProperty,
            &wm.name().to_owned(),
        )?;

        self.set_prop(
            support_win,
            self.atoms._NET_SUPPORTING_WM_CHECK,
            WindowProperty,
            &support_win,
        )?;
        self.set_prop(
            self.root_window,
            self.atoms._NET_SUPPORTING_WM_CHECK,
            WindowProperty,
            &support_win,
        )?;

        self.set_prop(
            self.root_window,
            self.atoms._NET_SUPPORTED,
            AtomListProperty,
            &self.atoms.net_atoms().into_iter().collect::<Vec<_>>(),
        )?;

        Ok(())
    }

    /// Subscribe to relevant events and call [`Wm::manage`].
    fn xmanage<'a>(&'a self, wm: &mut Wm<&'a Self>, c: Client<&'a Self>) -> WmResult<(), &'a Self> {
        let wid = c.window;
        wm.manage(c)?;

        self.conn
            .change_window_attributes(
                wid,
                &ChangeWindowAttributesAux::new().event_mask(
                    EventMask::ENTER_WINDOW // EnterNotify on root
                        | EventMask::LEAVE_WINDOW // LeaveNotify on root
                        | EventMask::PROPERTY_CHANGE // PropertyChange on root
                        | EventMask::POINTER_MOTION // MotionNotify on root
                        | EventMask::FOCUS_CHANGE, // FocusIn (TODO everywhere?)
                ),
            )
            .map_err(XError::from)?
            .check()
            .map_err(|err| XError::ReplyError {
                err,
                func: "change event mask on client".into(),
            })?;

        Ok(())
    }

    /// Adopt children of the root window.
    ///
    /// These can be unmanaged windows or clients
    /// left over when the previous WM quit.
    fn adopt_children<'a>(&'a self, wm: &mut Wm<&'a Self>) -> Result<(), FatalError> {
        let childs = self
            .conn
            .query_tree(self.root_window)
            .map_err(XError::from)
            .map_err(FatalError::XError)?
            .reply()
            .map_err(|err| XError::ReplyError {
                err,
                func: "get children of root window".into(),
            })
            .map_err(FatalError::XError)?
            .children;

        for child in childs {
            let (c, _is_focused) = match new_client(self, child) {
                Ok(c) => c,
                Err(err) => {
                    warn!(%err, wid = child, "cannot adopt window");
                    continue;
                }
            };
            if let Err(err) = self.xmanage(wm, c) {
                warn!(%err, wid = child, "cannot manage window");
                continue;
            }
        }
        Ok(())
    }
}

/// Yet another X tiling WM in Rust.
#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Cli {
    /// Don't register for the `SUBSTRUCTURE_REDIRECT` event.
    ///
    /// This is an unstable debug option.
    #[clap(short = 'R', long)]
    no_redirect: bool,

    /// Exit after startup is complete.
    ///
    /// This is an unstable debug option.
    #[clap(short = 's', long)]
    only_startup: bool,

    /// List managed clients.
    ///
    /// This is an unstable debug option.
    #[clap(short = 'm', long)]
    list_managed: bool,

    /// List unmanaged clients.
    ///
    /// This is an unstable debug option.
    #[clap(short = 'M', long)]
    list_unmanaged: bool,

    /// List visible clients.
    ///
    /// This is an unstable debug option.
    #[clap(short = 'v', long)]
    list_visible: bool,
}

/// Do initial setup and run the event loop.
fn main() {
    tracing_subscriber::fmt()
        .with_writer(io::stderr)
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    let f: fn() -> Result<(), FatalError> = || {
        let cli: Cli = Cli::parse();

        let (conn, screen_num) = x11rb::connect(None)?;

        let x = XDisplay::new(conn, screen_num)?;
        // TODO load config from file
        let mut wm =
            Wm::new(&x, &Config::default()).map_err(|err| FatalError::WmError(err.to_string()))?;

        debug!("wm initialized {:#?}", wm);

        x.register_wm(&wm, cli.no_redirect)
            .map_err(FatalError::XError)?;
        x.adopt_children(&mut wm)?;

        if cli.list_managed || cli.list_unmanaged || cli.list_visible {
            for client in wm.clients.values() {
                if (cli.list_managed && client.properties.contains(ClientProperty::Managed))
                    || (cli.list_unmanaged && !client.properties.contains(ClientProperty::Managed))
                    || (cli.list_visible && client.properties.contains(ClientProperty::Visible))
                {
                    println!("{:#?}", client);
                }
            }
        }

        if cli.only_startup {
            return Ok(());
        }

        loop {
            let ev = x
                .conn
                .wait_for_event()
                .map_err(XError::from)
                .map_err(FatalError::XError)?;

            let mut f = || {
                match &ev {
                    // Window changes
                    Event::MapNotify(ev) => {
                        let (client, _) = new_client(&x, ev.window)?;
                        x.xmanage(&mut wm, client)?;
                    }
                    Event::MapRequest(ev) => {
                        let (client, _) = new_client(&x, ev.window)?;
                        x.xmanage(&mut wm, client)?;
                    }
                    Event::ConfigureNotify(ev) => {
                        if ev.window == x.root_window {
                            wm.screen_geometry_changed(Geometry {
                                pos: Coordinates {
                                    x: ev.x as i32,
                                    y: ev.y as i32,
                                },
                                size: mawm_core::Dimensions {
                                    w: ev.width as i32,
                                    h: ev.height as i32,
                                },
                            })?;
                        } else {
                            wm.client_geometry_changed(
                                ev.window,
                                Geometry {
                                    pos: Coordinates {
                                        x: ev.x as i32,
                                        y: ev.y as i32,
                                    },
                                    size: mawm_core::Dimensions {
                                        w: ev.width as i32,
                                        h: ev.height as i32,
                                    },
                                },
                            )?;
                        }
                    }
                    Event::ConfigureRequest(ev) => {
                        wm.client_geometry_change_requested(
                            ev.window,
                            Geometry {
                                pos: Coordinates {
                                    x: ev.x as i32,
                                    y: ev.y as i32,
                                },
                                size: mawm_core::Dimensions {
                                    w: ev.width as i32,
                                    h: ev.height as i32,
                                },
                            },
                        )?;
                    }
                    Event::DestroyNotify(ev) => {
                        wm.unmanage(ev.window)?;
                    }
                    Event::UnmapNotify(evv) => {
                        wm.unmanage(evv.window)?;
                    }
                    Event::PropertyNotify(ev) => {
                        handle_property_notify(&x, &mut wm, ev)?;
                    }
                    Event::ClientMessage(ev) => {
                        handle_client_message(&x, &mut wm, ev)?;
                    }

                    // Focus related events
                    Event::EnterNotify(ev) => {
                        if ev.mode != NotifyMode::NORMAL || ev.detail == NotifyDetail::INFERIOR {
                            return Ok(());
                        }

                        wm.mouse_entered_client(ev.event)?;
                    }
                    Event::LeaveNotify(ev) => {
                        if ev.mode != NotifyMode::NORMAL || ev.detail == NotifyDetail::INFERIOR {
                            return Ok(());
                        }

                        wm.mouse_left_client()?;
                    }
                    Event::FocusIn(ev) => {
                        wm.focus_changed(ev.event)?;
                    }
                    Event::MotionNotify(ev) => {
                        if !ev.same_screen {
                            return Ok(());
                        }

                        wm.mouse_moved(Coordinates {
                            x: ev.root_x as i32,
                            y: ev.root_y as i32,
                        })?;
                    }
                    Event::ButtonPress(ev) => {
                        if !ev.same_screen {
                            return Ok(());
                        }

                        wm.mouse_clicked(Coordinates {
                            x: ev.root_x as i32,
                            y: ev.root_y as i32,
                        })?;
                    }

                    // Error
                    Event::Error(err) => {
                        warn!(?err, "x errror");
                    }
                    _ => {
                        trace!(?ev, "unhandled event");
                    }
                };
                Ok(())
            };
            if let Err(err) = f() {
                let _: WmError<XError> = err;
                warn!(?err, ?ev, "{}", err);
            }
        }
    };

    if let Err(err) = f() {
        error!(?err, "{}", err);
        exit(1);
    }
}

/// A fatal error that kills the whole window manager.
#[derive(Debug, Error)]
pub enum FatalError {
    /// Failed to connect to the X server.
    #[error("failed to connect to the X server: {0}")]
    ConnectError(#[from] ConnectError),

    /// Failed to connect to the X server.
    #[error("{0}")]
    WmError(String),

    /// XError.
    #[error("{0}")]
    XError(#[source] XError),
}

/// An error the window manager can handle.
#[derive(Debug, Error)]
pub enum XError {
    /// Failed to communicate with the X server.
    #[error("failed to communicate with the X server: {0}")]
    ConnectionError(#[from] ConnectionError),

    /// Request failed.
    #[error("failed to {func}")]
    ReplyError {
        /// Underlying reply error.
        #[source]
        err: ReplyError,

        /// The action that caused the error (e.g. `intern atoms`).
        func: Cow<'static, str>,
    },

    /// All IDs are exhausted.
    #[error("could not allocate X id because all were exhausted")]
    IdsExhausted,

    /// Property was in an unexpected format.
    #[error("failed to convert {prop} ({typ}/{format}) to {want_typ}/{want_format}")]
    UnexpectedFormat {
        /// The property requested.
        prop: String,
        /// The actual type of the property.
        typ: String,
        /// The actual format of the property.
        format: u8,
        /// The expected type.
        want_typ: String,
        /// The expected format.
        want_format: u8,
    },
}

impl From<ReplyOrIdError> for XError {
    fn from(e: ReplyOrIdError) -> Self {
        match e {
            ReplyOrIdError::IdsExhausted => Self::IdsExhausted,
            e => Self::from(e),
        }
    }
}

impl DisplayError for XError {}

impl Debug for XDisplay {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("XDisplay")
            .field("root_window", &self.root_window)
            .finish()
    }
}
