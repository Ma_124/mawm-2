//! Create new [`Client`]s from window ids using [`new_client`].

use mawm_core::{
    Client, ClientProperties, ClientProperty, ClientType, Coordinates, Dimensions, Geometry, Wm,
    WmResult,
};
use tracing::warn;
use x11rb::protocol::xproto::{
    AtomEnum, ClientMessageEvent, ConnectionExt, GetGeometryReply, MapState, PropertyNotifyEvent,
    Window,
};

use crate::{
    properties::{
        net_state_to_property, CardinalProperty, NetWmStateProperty, NetWmTypeProperty,
        StringProperty, WindowProperty, WmClassProperty, WmHintsProperty, WmSizeHintsProperty,
        WmStateProperty,
    },
    XDisplay, XError,
};

/// Returns a new [`Client`] struct from a window id as well as whether that window was focused.
pub fn new_client(x: &XDisplay, w: Window) -> Result<(Client<&'_ XDisplay>, bool), XError> {
    // Start async requests
    let attrs = x.conn.get_window_attributes(w)?;
    let class = x.get_prop(w, AtomEnum::WM_CLASS, WmClassProperty)?;
    let pid = x.get_prop(w, x.atoms._NET_WM_PID, CardinalProperty)?;
    let title = x.get_prop(w, AtomEnum::WM_NAME, StringProperty)?;
    let typ = x.get_prop(w, x.atoms._NET_WM_WINDOW_TYPE, NetWmTypeProperty)?;
    let parent = x.get_prop(w, AtomEnum::WM_TRANSIENT_FOR, WindowProperty)?;
    let wm_state = x.get_prop(w, x.atoms.WM_STATE, WmStateProperty)?;
    let net_state = x.get_prop(w, x.atoms._NET_WM_STATE, NetWmStateProperty)?;
    let hints = x.get_prop(w, AtomEnum::WM_HINTS, WmHintsProperty)?;
    let size_hints = x.get_prop(w, AtomEnum::WM_NORMAL_HINTS, WmSizeHintsProperty)?;
    let geometry = x.conn.get_geometry(w)?;

    // Await responses
    let attrs = attrs.reply().map_err(|err| XError::ReplyError {
        err,
        func: "get window attributes".into(),
    })?;
    let (instance_id, app_id) = class.reply()?.unwrap_or_default();
    let parent = parent.reply()?;
    let typ = match typ.reply()? {
        Some(typ) => typ,
        None if attrs.override_redirect => ClientType::Normal,
        None if parent.is_some() => ClientType::Dialog,
        _ => ClientType::Normal,
    };
    let (wm_state_props, _) = wm_state.reply()?.unwrap_or_default();
    let (net_state_props, focused) = net_state.reply()?.unwrap_or_default();
    let (group_leader, hints_props) = hints.reply()?.unwrap_or_default();
    let geometry = geometry_from_reply(geometry.reply().map_err(|err| XError::ReplyError {
        err,
        func: "get window geometry".into(),
    })?);

    let mut extra_props = ClientProperties::empty();

    if !attrs.override_redirect {
        extra_props |= ClientProperty::Managed;
    }

    if attrs.map_state == MapState::VIEWABLE {
        extra_props |= ClientProperty::Visible;
    }

    if attrs.map_state == MapState::UNVIEWABLE {
        // ancestor is unmapped
        // which should never happen
        // because it's a child of the root window
        warn!(wid = w, "window is unviewable");
    }

    Ok((
        Client {
            window: w,
            app_id,
            instance_id,
            process: pid.reply()?.unwrap_or_default(),
            title: title.reply()?.unwrap_or_default(),
            typ,
            parent,
            properties: wm_state_props | net_state_props | hints_props | extra_props,
            geometry,
            old_geometry: None,
            size_hints: size_hints.reply()?.unwrap_or_default(),
            group_leader,
        },
        focused,
    ))
}

/// Handle a X property change.
pub fn handle_property_notify<'a>(
    x: &'a XDisplay,
    wm: &mut Wm<&'a XDisplay>,
    ev: &PropertyNotifyEvent,
) -> WmResult<(), &'a XDisplay> {
    if ev.atom == x.atoms._NET_WM_NAME {
        wm.client_title_changed(
            ev.window,
            x.get_prop(ev.window, x.atoms._NET_WM_NAME, StringProperty)?
                .reply()?
                .unwrap_or_default(),
        )?;
    } else if ev.atom == x.atoms._NET_WM_WINDOW_TYPE {
        // TODO proper default for _NET_WM_WINDOW_TYPE
        wm.client_type_changed(
            ev.window,
            x.get_prop(ev.window, x.atoms._NET_WM_WINDOW_TYPE, NetWmTypeProperty)?
                .reply()?
                .unwrap_or(ClientType::Normal),
        )?;
    } else if ev.atom == u32::from(AtomEnum::WM_TRANSIENT_FOR) {
        wm.client_parent_changed(
            ev.window,
            x.get_prop(ev.window, AtomEnum::WM_TRANSIENT_FOR, WindowProperty)?
                .reply()?,
        )?;
    } else if ev.atom == u32::from(AtomEnum::WM_NORMAL_HINTS) {
        wm.client_size_hints_changed(
            ev.window,
            x.get_prop(ev.window, AtomEnum::WM_NORMAL_HINTS, WmSizeHintsProperty)?
                .reply()?
                .unwrap_or_default(),
        )?;
    }
    Ok(())
}

/// Handle an X client message.
pub fn handle_client_message<'a>(
    x: &'a XDisplay,
    wm: &mut Wm<&'a XDisplay>,
    ev: &ClientMessageEvent,
) -> WmResult<(), &'a XDisplay> {
    if ev.type_ == x.atoms._NET_ACTIVE_WINDOW {
        wm.client_wants_focus(ev.type_)?;
    } else if ev.type_ == x.atoms._NET_WM_STATE {
        let props = net_state_to_property(x, ev.data.as_data32()[1])
            .map(ClientProperties::from)
            .unwrap_or_default()
            | net_state_to_property(x, ev.data.as_data32()[2])
                .map(ClientProperties::from)
                .unwrap_or_default();
        if let Some(c) = wm.clients.get(&ev.window) {
            let new_props = match ev.data.as_data32()[0] {
                // Remove property
                0 => c.properties & !props,
                // Add property
                1 => c.properties | props,
                // Toggle property
                2 => c.properties ^ props,
                _ => c.properties,
            };
            wm.set_client_properties(ev.window, new_props)?;
        };
    }
    Ok(())
}

fn geometry_from_reply(reply: GetGeometryReply) -> Geometry {
    Geometry {
        pos: Coordinates {
            x: reply.x as i32,
            y: reply.y as i32,
        },
        size: Dimensions {
            w: reply.width as i32,
            h: reply.height as i32,
        },
    }
}
