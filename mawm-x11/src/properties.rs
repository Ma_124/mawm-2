//! Provide functions to interact with X properties.

use std::borrow::Cow;

use mawm_core::{ClientProperties, ClientProperty, ClientType, Dimensions, Ratio, SizeHints};
use x11rb::{
    cookie::{Cookie, VoidCookie},
    properties::{WmHints, WmSizeHints},
    protocol::xproto::{Atom, AtomEnum, ConnectionExt, GetPropertyReply, PropMode, Window},
    rust_connection::{ReplyError, RustConnection},
};

use crate::{XDisplay, XError};

const MAX_LEN: u32 = 1024;

/// A type of X property.
pub trait PropertyType: Copy + Clone {
    /// The rust type.
    type Value;

    /// Maximum length in multiples of [`Self::want_format`].
    ///
    /// With `max_len = 1` and `want_format = 32`, `4` bytes will be requested.
    fn max_len(&self) -> u32;

    /// The type to request.
    fn request_type(&self, x: &XDisplay) -> Atom;

    /// The expected type (only for error messages).
    fn want_type_string(&self) -> &'static str;

    /// The expected format (only for error messages).
    fn want_format(&self) -> u8;

    /// Parse the reply.
    ///
    /// If the reply supplies the wrong type or format return [`None`].
    fn parse(&self, x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError>;

    /// Construct the data.
    fn construct<'a>(
        &self,
        x: &XDisplay,
        inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError>;
}

/// A handle to a response from the X server about a property.
pub struct PropertyCookie<'a, T> {
    t: T,
    x: &'a XDisplay,
    prop: Atom,
    cookie: Cookie<'a, RustConnection, GetPropertyReply>,
}

impl<'a, T: PropertyType> PropertyCookie<'a, T> {
    /// Get the reply.
    pub fn reply(self) -> Result<Option<T::Value>, XError> {
        let r = self.cookie.reply().map_err(|err| XError::ReplyError {
            err,
            func: format!("get atom {}", self.x.atoms.format_name(self.prop)).into(),
        })?;

        let got_type = r.type_;
        let got_format = r.format;

        if let Some(out) = self.t.parse(self.x, r)? {
            return Ok(Some(out));
        }

        if got_format == 0 {
            return Ok(None);
        }

        Err(XError::UnexpectedFormat {
            prop: self.x.atoms.format_name(self.prop),
            typ: self.x.atoms.format_name(got_type),
            format: got_format,
            want_typ: self.t.want_type_string().to_owned(),
            want_format: self.t.want_format(),
        })
    }
}

/// Reads any string and writes a `UTF8_STRING`.
#[derive(Clone, Copy)]
pub struct StringProperty;

impl PropertyType for StringProperty {
    type Value = String;

    fn max_len(&self) -> u32 {
        MAX_LEN
    }

    fn request_type(&self, _x: &XDisplay) -> Atom {
        AtomEnum::ANY.into()
    }

    fn want_type_string(&self) -> &'static str {
        "STRING | UTF8_STRING"
    }

    fn want_format(&self) -> u8 {
        8
    }

    fn parse(&self, x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if r.type_ != u32::from(AtomEnum::STRING) && r.type_ != x.atoms.UTF8_STRING && r.format != 8
        {
            return Ok(None);
        }

        let mut bytes = r.value8().unwrap().collect::<Vec<_>>();
        // Trim trailing NULL bytes
        while let Some(l) = bytes.as_slice().last() {
            if *l != 0x00 {
                break;
            }
            bytes.pop();
        }
        Ok(Some(String::from_utf8_lossy(&bytes).to_string()))
    }

    fn construct<'a>(
        &self,
        x: &XDisplay,
        inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        Ok((x.atoms.UTF8_STRING, inp.as_bytes().into()))
    }
}

/// Reads and writes the WM_CLASS (instance, class).
#[derive(Clone, Copy)]
pub struct WmClassProperty;

impl PropertyType for WmClassProperty {
    type Value = (String, String);

    fn max_len(&self) -> u32 {
        StringProperty.max_len()
    }

    fn request_type(&self, x: &XDisplay) -> Atom {
        StringProperty.request_type(x)
    }

    fn want_type_string(&self) -> &'static str {
        StringProperty.want_type_string()
    }

    fn want_format(&self) -> u8 {
        StringProperty.want_format()
    }

    fn parse(&self, x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if let Some(mut instance) = StringProperty.parse(x, r)? {
            if let Some(nul_index) = instance.find('\0') {
                let class = instance[nul_index + 1..].to_owned();
                instance.truncate(nul_index);
                Ok(Some((instance, class)))
            } else {
                Ok(Some((instance, "".to_owned())))
            }
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        Ok((
            AtomEnum::WM_CLASS.into(),
            format!("{}\0{}\0", inp.0, inp.1).into_bytes().into(),
        ))
    }
}

/// Reads and writes CARDINALs ([`u32`]).
#[derive(Clone, Copy)]
pub struct CardinalProperty;

impl PropertyType for CardinalProperty {
    type Value = u32;

    fn max_len(&self) -> u32 {
        1
    }

    fn request_type(&self, _x: &XDisplay) -> Atom {
        AtomEnum::CARDINAL.into()
    }

    fn want_type_string(&self) -> &'static str {
        "CARDINAL"
    }

    fn want_format(&self) -> u8 {
        32
    }

    fn parse(&self, _x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if r.format == 32 && r.type_ == u32::from(AtomEnum::CARDINAL) {
            Ok(r.value32().unwrap().next())
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        Ok((
            AtomEnum::CARDINAL.into(),
            Vec::from(inp.to_ne_bytes()).into(),
        ))
    }
}

/// Reads and writes WINDOWs.
#[derive(Clone, Copy)]
pub struct WindowProperty;

impl PropertyType for WindowProperty {
    type Value = u32;

    fn max_len(&self) -> u32 {
        1
    }

    fn request_type(&self, _x: &XDisplay) -> Atom {
        AtomEnum::WINDOW.into()
    }

    fn want_type_string(&self) -> &'static str {
        "WINDOW"
    }

    fn want_format(&self) -> u8 {
        32
    }

    fn parse(&self, _x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if r.format == 32 && r.type_ == u32::from(AtomEnum::WINDOW) {
            Ok(r.value32().unwrap().next())
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        Ok((AtomEnum::WINDOW.into(), Vec::from(inp.to_ne_bytes()).into()))
    }
}

/// Reads and writes lists of ATOMs (`Vec<Atom>`).
#[derive(Clone, Copy)]
pub struct AtomListProperty;

impl PropertyType for AtomListProperty {
    type Value = Vec<u32>;

    fn max_len(&self) -> u32 {
        MAX_LEN
    }

    fn request_type(&self, _x: &XDisplay) -> Atom {
        AtomEnum::ATOM.into()
    }

    fn want_type_string(&self) -> &'static str {
        "ATOM list"
    }

    fn want_format(&self) -> u8 {
        32
    }

    fn parse(&self, _x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if r.format == 32 && r.type_ == u32::from(AtomEnum::ATOM) {
            Ok(Some(r.value32().unwrap().collect::<Vec<_>>()))
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        Ok((
            AtomEnum::ATOM.into(),
            inp.as_slice()
                .iter()
                .flat_map(|i| i.to_ne_bytes())
                .collect::<Vec<_>>()
                .into(),
        ))
    }
}

/// Reads a [`ClientType`] from a list of `_NET_WM_TYPE` atoms.
#[derive(Clone, Copy)]
pub struct NetWmTypeProperty;

impl PropertyType for NetWmTypeProperty {
    type Value = ClientType;

    fn max_len(&self) -> u32 {
        AtomListProperty.max_len()
    }

    fn request_type(&self, x: &XDisplay) -> Atom {
        AtomListProperty.request_type(x)
    }

    fn want_type_string(&self) -> &'static str {
        AtomListProperty.want_type_string()
    }

    fn want_format(&self) -> u8 {
        AtomListProperty.want_format()
    }

    fn parse(&self, x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if let Some(candidates) = AtomListProperty.parse(x, r)? {
            for a in candidates {
                if let Some(typ) = if a == x.atoms._NET_WM_DESKTOP {
                    Some(ClientType::Desktop)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_DOCK {
                    Some(ClientType::Dock)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_TOOLBAR {
                    Some(ClientType::Toolbar)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_MENU {
                    Some(ClientType::Menu)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_UTILITY {
                    Some(ClientType::Utility)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_SPLASH {
                    Some(ClientType::Splash)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_DIALOG {
                    Some(ClientType::Dialog)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_DROPDOWN_MENU {
                    Some(ClientType::DropdownMenu)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_POPUP_MENU {
                    Some(ClientType::PopupMenu)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_TOOLTIP {
                    Some(ClientType::Tooltip)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_NOTIFICATION {
                    Some(ClientType::Notification)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_COMBO {
                    Some(ClientType::Combo)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_DND {
                    Some(ClientType::DragNDrop)
                } else if a == x.atoms._NET_WM_WINDOW_TYPE_NORMAL {
                    Some(ClientType::Normal)
                } else {
                    None
                } {
                    return Ok(Some(typ));
                }
            }
        }
        Ok(None)
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        _inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        unimplemented!()
    }
}

/// Reads [`ClientProperties`] and `focused` from a list of `WM_STATE` atoms.
/// **Not to be confused with [`NetWmStateProperty`]**
#[derive(Clone, Copy)]
pub struct WmStateProperty;

impl PropertyType for WmStateProperty {
    type Value = (ClientProperties, Window);

    fn max_len(&self) -> u32 {
        2
    }

    fn request_type(&self, x: &XDisplay) -> Atom {
        x.atoms.WM_STATE
    }

    fn want_type_string(&self) -> &'static str {
        "WM_STATE"
    }

    fn want_format(&self) -> u8 {
        32
    }

    fn parse(&self, x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if r.format == 32 && r.type_ == x.atoms.WM_STATE {
            let mut i = r.value32().unwrap();
            let state = i.next().unwrap();
            let icon = i.next().unwrap_or_default();

            // Iconic state
            if state == 3 {
                Ok(Some((ClientProperty::Minimized.into(), icon)))
            } else {
                Ok(Some((ClientProperties::empty(), icon)))
            }
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        x: &XDisplay,
        inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        let (props, icon) = inp;
        let mut v = Vec::new();
        if props.contains(ClientProperty::Minimized) {
            // IconicState
            v.extend_from_slice(&3_u32.to_ne_bytes());
        } else {
            // NormalState
            v.extend_from_slice(&1_u32.to_ne_bytes());
        }
        v.extend_from_slice(&icon.to_ne_bytes());
        Ok((x.atoms.WM_STATE, v.into()))
    }
}

/// Reads [`ClientProperties`] and `focused` from a list of `_NET_WM_STATE` atoms.
/// **Not to be confused with [`WmStateProperty`]**
#[derive(Clone, Copy)]
pub struct NetWmStateProperty;

impl PropertyType for NetWmStateProperty {
    type Value = (ClientProperties, bool);

    fn max_len(&self) -> u32 {
        AtomListProperty.max_len()
    }

    fn request_type(&self, x: &XDisplay) -> Atom {
        AtomListProperty.request_type(x)
    }

    fn want_type_string(&self) -> &'static str {
        AtomListProperty.want_type_string()
    }

    fn want_format(&self) -> u8 {
        AtomListProperty.want_format()
    }

    fn parse(&self, x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if let Some(atoms) = AtomListProperty.parse(x, r)? {
            let mut props = ClientProperties::empty();
            let mut focused = false;
            for a in atoms {
                props |= net_state_to_property(x, a)
                    .map(ClientProperties::from)
                    .unwrap_or_else(ClientProperties::empty);
                if a == x.atoms._NET_WM_STATE_FOCUSED {
                    focused = true;
                }
            }
            Ok(Some((props, focused)))
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        _inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        unimplemented!()
    }
}

/// Convert a `_NET_WM_STATE_*` [`Atom`] to a [`ClientProperty`].
pub fn net_state_to_property(x: &XDisplay, a: Atom) -> Option<ClientProperty> {
    if a == x.atoms._NET_WM_STATE_MODAL {
        Some(ClientProperty::Modal)
    } else if a == x.atoms._NET_WM_STATE_MAXIMIZED_HORZ || a == x.atoms._NET_WM_STATE_MAXIMIZED_VERT
    {
        Some(ClientProperty::Fullscreen)
    } else if a == x.atoms._NET_WM_STATE_HIDDEN {
        Some(ClientProperty::Minimized)
    } else if a == x.atoms._NET_WM_STATE_FULLSCREEN {
        Some(ClientProperty::Fullscreen)
    } else if a == x.atoms._NET_WM_STATE_ABOVE {
        Some(ClientProperty::AboveNormal)
    } else if a == x.atoms._NET_WM_STATE_BELOW {
        Some(ClientProperty::BelowNormal)
    } else if a == x.atoms._NET_WM_STATE_DEMANDS_ATTENTION {
        Some(ClientProperty::DemandsAttention)
    } else if a == x.atoms._NET_WM_STATE_STICKY {
        Some(ClientProperty::Sticky)
    } else {
        None
    }
}

/// Reads a window group leader and [`ClientProperties`] from `WM_HINTS`.
#[derive(Clone, Copy)]
pub struct WmHintsProperty;

impl PropertyType for WmHintsProperty {
    type Value = (Option<Window>, ClientProperties);

    fn max_len(&self) -> u32 {
        // NUM_WM_HINTS_ELEMENTS (num of u32s) from
        //   https://github.com/psychon/x11rb/blob/a6cc0d1/src/properties.rs#L450
        9
    }

    fn request_type(&self, _x: &XDisplay) -> Atom {
        AtomEnum::WM_HINTS.into()
    }

    fn want_type_string(&self) -> &'static str {
        "WM_HINTS"
    }

    fn want_format(&self) -> u8 {
        32
    }

    fn parse(&self, _x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if r.type_ == u32::from(AtomEnum::WM_HINTS) {
            let hints = WmHints::from_reply(&r)
                .map_err(ReplyError::from)
                .map_err(|err| XError::ReplyError {
                    err,
                    func: "parse WM_HINTS".into(),
                })?;
            let mut props = ClientProperties::empty();
            if hints.urgent {
                props |= ClientProperty::Urgent;
            }
            if matches!(hints.input, Some(false)) {
                props |= ClientProperty::NeverFocus;
            }
            // TODO expose initial state
            Ok(Some((hints.window_group, props)))
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        _inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        unimplemented!()
    }
}

/// Reads [`SizeHints`] from properties with type `WM_SIZE_HINTS`.
#[derive(Clone, Copy)]
pub struct WmSizeHintsProperty;

impl PropertyType for WmSizeHintsProperty {
    type Value = SizeHints;

    fn max_len(&self) -> u32 {
        // NUM_WM_SIZE_HINTS_ELEMENTS (num of u32s) from
        //   https://github.com/psychon/x11rb/blob/a6cc0d1/src/properties.rs#L164
        18
    }

    fn request_type(&self, _x: &XDisplay) -> Atom {
        AtomEnum::WM_SIZE_HINTS.into()
    }

    fn want_type_string(&self) -> &'static str {
        "WM_SIZE_HINTS"
    }

    fn want_format(&self) -> u8 {
        32
    }

    fn parse(&self, _x: &XDisplay, r: GetPropertyReply) -> Result<Option<Self::Value>, XError> {
        if r.type_ == u32::from(AtomEnum::WM_SIZE_HINTS) {
            let hints = WmSizeHints::from_reply(&r)
                .map_err(ReplyError::from)
                .map_err(|err| XError::ReplyError {
                    err,
                    func: "parse WM_SIZE_HINTS".into(),
                })?;
            Ok(Some(SizeHints {
                min_size: hints.min_size.map(|(w, h)| Dimensions { w, h }),
                max_size: hints.max_size.map(|(w, h)| Dimensions { w, h }),
                min_max_aspect_ratio: hints.aspect.map(|(min, max)| {
                    (
                        Ratio::new(min.numerator, min.denominator),
                        Ratio::new(max.numerator, max.denominator),
                    )
                }),
                size_base: hints.base_size.map(|(w, h)| Dimensions { w, h }),
                size_increment: hints.size_increment.map(|(w, h)| Dimensions { w, h }),
            }))
        } else {
            Ok(None)
        }
    }

    fn construct<'a>(
        &self,
        _x: &XDisplay,
        _inp: &'a Self::Value,
    ) -> Result<(Atom, Cow<'a, [u8]>), XError> {
        unimplemented!()
    }
}

impl XDisplay {
    /// Get a property.
    pub fn get_prop<T: PropertyType>(
        &self,
        w: Window,
        prop: impl Into<Atom>,
        typ: T,
    ) -> Result<PropertyCookie<T>, XError> {
        let prop = prop.into();
        Ok(PropertyCookie {
            t: typ,
            x: self,
            prop,
            cookie: self.conn.get_property(
                false,
                w,
                prop,
                typ.request_type(self),
                0,
                div_ceil(typ.max_len() * (typ.want_format() / 8) as u32, 4),
            )?,
        })
    }

    /// Set a property.
    pub fn set_prop<T: PropertyType>(
        &self,
        w: Window,
        prop: impl Into<Atom>,
        typ: T,
        val: &T::Value,
    ) -> Result<VoidCookie<'_, RustConnection>, XError> {
        let (type_atom, data) = typ.construct(self, val)?;
        Ok(self.conn.change_property(
            PropMode::REPLACE,
            w,
            prop,
            type_atom,
            typ.want_format(),
            data.len() as u32 / (typ.want_format() / 8) as u32,
            &data,
        )?)
    }

    /// Delete a property.
    pub fn delete_prop(
        &self,
        w: Window,
        prop: impl Into<Atom>,
    ) -> Result<VoidCookie<'_, RustConnection>, XError> {
        Ok(self.conn.delete_property(w, prop.into())?)
    }
}

fn div_ceil(dividend: u32, divisor: u32) -> u32 {
    (dividend + (divisor - 1)) / divisor
}
